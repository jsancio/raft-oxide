use rand;
use std::cmp::max;
use std::cmp::min;
use std::collections::HashMap;
use std::collections::HashSet;
use std::fmt::Debug;
use std::io;
use std::sync::mpsc;
use std::thread;
use std::time::Duration;

trait Operation<I, O> {
    fn apply(&self, input: &mut I) -> O;
}

// TODO: Replace this with a more interesting implemmentation
impl Operation<i64, i64> for i64 {
    fn apply(&self, input: &mut i64) -> i64 {
        *input += self;
        *input
    }
}

#[derive(Debug, Clone)]
struct Log<T> {
    term: u64,
    operation: T,
}

impl<T> Log<T> {
    fn new(term: u64, operation: T) -> Log<T> {
        Log { term, operation }
    }
}

#[derive(Debug)]
struct VoteRequest {
    term: u64,
    candidate_id: u8,
    last_log_index: usize,
    last_log_term: u64,
}

impl VoteRequest {
    fn new(term: u64, candidate_id: u8, last_log_index: usize, last_log_term: u64) -> VoteRequest {
        VoteRequest {
            term,
            candidate_id,
            last_log_index,
            last_log_term,
        }
    }
}

#[derive(Debug)]
struct VoteResponse {
    term: u64,
    vote_granted: bool,
}

impl VoteResponse {
    fn new(term: u64, vote_granted: bool) -> VoteResponse {
        VoteResponse { term, vote_granted }
    }
}

#[derive(Debug, Clone)]
struct AppendEntriesRequest<T> {
    term: u64,
    leader_id: u8,
    prev_log_index: usize,
    prev_log_term: u64,
    entries: Vec<Log<T>>,
    leader_commit: usize,
}

impl<T> AppendEntriesRequest<T> {
    fn new(
        term: u64,
        leader_id: u8,
        prev_log_index: usize,
        prev_log_term: u64,
        entries: Vec<Log<T>>,
        leader_commit: usize,
    ) -> AppendEntriesRequest<T> {
        AppendEntriesRequest {
            term,
            leader_id,
            prev_log_index,
            prev_log_term,
            entries,
            leader_commit,
        }
    }
}

#[derive(Debug)]
struct AppendEntriesResponse {
    term: u64,
    success: bool,
}

impl AppendEntriesResponse {
    fn new(term: u64, success: bool) -> AppendEntriesResponse {
        AppendEntriesResponse { term, success }
    }
}

#[derive(Debug)]
struct AddLogRequest<T> {
    operation: T,
}

impl<T> AddLogRequest<T> {
    fn new(operation: T) -> AddLogRequest<T> {
        AddLogRequest { operation }
    }
}

#[derive(Debug)]
struct AddLogResponse<O> {
    leader: Option<u8>,
    output: Option<O>,
}

impl<O> AddLogResponse<O> {
    fn new(leader: Option<u8>, output: Option<O>) -> AddLogResponse<O> {
        AddLogResponse { leader, output }
    }
}

#[derive(Debug)]
struct ClientResponse<O>(u64, AddLogResponse<O>);

#[derive(Debug)]
enum RaftRequest<T> {
    Vote(VoteRequest),
    AppendEntries(AppendEntriesRequest<T>),
}

#[derive(Debug)]
enum RaftResponse {
    Vote(VoteResponse),
    AppendEntries(AppendEntriesResponse),
}

#[derive(Debug)]
enum RaftMessage<T, O> {
    Request(u64, RaftRequest<T>, mpsc::Sender<RaftMessage<T, O>>),
    Response(u64, RaftResponse),
    ClientRequest(u64, AddLogRequest<T>, mpsc::Sender<ClientResponse<O>>),
}

struct Follower {
    leader_id: Option<u8>,
}

impl Follower {
    fn new(leader_id: Option<u8>) -> Follower {
        Follower { leader_id }
    }

    fn message<T, I, O>(
        &mut self,
        share: &mut Share<T, I>,
        message: RaftMessage<T, O>,
    ) -> io::Result<()>
    where
        T: Debug,
        O: Debug,
    {
        match message {
            RaftMessage::Request(request_id, RaftRequest::Vote(request), reply) => {
                let vote_granted = if request.term < share.current_term {
                    false
                } else if let Some(voted_for) = share.voted_for {
                    if voted_for == request.candidate_id {
                        true
                    } else {
                        false
                    }
                } else if request.last_log_term > share.last_log_term() {
                    true
                } else if request.last_log_term == share.last_log_term()
                    && request.last_log_index >= share.last_log_index()
                {
                    true
                } else {
                    false
                };

                if vote_granted {
                    share.current_term = request.term;
                    share.voted_for = Some(request.candidate_id);
                };

                send(
                    &reply,
                    RaftMessage::Response(
                        request_id,
                        RaftResponse::Vote(VoteResponse::new(share.current_term, vote_granted)),
                    ),
                )
            }
            RaftMessage::Request(request_id, RaftRequest::AppendEntries(request), reply) => {
                let success = if request.term < share.current_term {
                    false
                } else if request.prev_log_index == 0 {
                    true
                } else if let Some(log) = share.get_log(request.prev_log_index) {
                    if log.term != request.prev_log_term {
                        false
                    } else {
                        true
                    }
                } else {
                    false
                };

                if success {
                    self.leader_id = Some(request.leader_id);
                    share.current_term = request.term;

                    share.logs.truncate(request.prev_log_index);
                    share.logs.extend(request.entries);

                    if request.leader_commit > share.commit_index {
                        share.commit_index = min(request.leader_commit, share.last_log_index());
                    }
                };

                send(
                    &reply,
                    RaftMessage::Response(
                        request_id,
                        RaftResponse::AppendEntries(AppendEntriesResponse::new(
                            share.current_term,
                            success,
                        )),
                    ),
                )
            }
            RaftMessage::Response(_, RaftResponse::Vote(response)) => {
                share.current_term = max(response.term, share.current_term);
                Ok(())
            }
            RaftMessage::Response(_, RaftResponse::AppendEntries(response)) => {
                share.current_term = max(response.term, share.current_term);
                Ok(())
            }
            RaftMessage::ClientRequest(request_id, _, reply) => send(
                &reply,
                ClientResponse(request_id, AddLogResponse::new(self.leader_id, None)),
            ),
        }
    }
}

struct Candidate {
    requests: HashMap<u64, u8>,
    votes: HashSet<u8>,
}

impl Candidate {
    fn new(id: u8) -> Candidate {
        let mut votes = HashSet::new();
        votes.insert(id);

        Candidate {
            requests: HashMap::new(),
            votes,
        }
    }

    fn update_votes(&mut self, id: &u64) -> usize {
        if let Some(peer_id) = self.requests.remove(id) {
            self.votes.insert(peer_id);
        }
        self.votes.len()
    }

    fn request_votes<'a, T, I, O, Q>(
        &mut self,
        share: &Share<T, I>,
        nodes: Q,
        reply: &mpsc::Sender<RaftMessage<T, O>>,
    ) -> io::Result<()>
    where
        T: 'a + Debug,
        O: 'a + Debug,
        Q: Iterator<Item = (&'a u8, &'a mpsc::Sender<RaftMessage<T, O>>)>,
    {
        for (peer_id, sender) in nodes {
            let request_id = rand::random();

            self.requests.insert(request_id, *peer_id);

            send(
                sender,
                RaftMessage::Request(
                    request_id,
                    RaftRequest::Vote(VoteRequest::new(
                        share.current_term,
                        share.id,
                        share.last_log_index(),
                        share.last_log_term(),
                    )),
                    reply.clone(),
                ),
            )?
        }

        Ok(())
    }
}

struct Leader<O> {
    // request_id -> (peer_id, prev_log_index, entries length)
    requests: HashMap<u64, (u8, usize, usize)>,
    client_requests: HashMap<usize, (u64, mpsc::Sender<ClientResponse<O>>)>,
    next_index: HashMap<u8, usize>,
    match_index: HashMap<u8, usize>,
}

impl<O> Leader<O>
where
    O: Debug,
{
    // TODO: Technically, we don't need to know about T if we pass the parameters explicitly.
    fn new<T, I>(share: &Share<T, I>, cluster: &Cluster<T, O>) -> Leader<O> {
        let mut next_index = HashMap::new();
        let mut match_index = HashMap::new();
        for &peer_id in cluster.nodes.keys().filter(|&&peer_id| peer_id != share.id) {
            next_index.insert(peer_id, share.last_log_index() + 1);
            match_index.insert(peer_id, 0);
        }

        Leader {
            requests: HashMap::new(),
            client_requests: HashMap::new(),
            next_index,
            match_index,
        }
    }

    fn client_request<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
        request_id: u64,
        request: AddLogRequest<T>,
        reply: mpsc::Sender<ClientResponse<O>>,
    ) -> io::Result<()>
    where
        T: Clone + Debug,
    {
        let index = share.push_log(request.operation);
        self.client_requests.insert(index, (request_id, reply));

        self.replicate_log(share, cluster)
    }

    // TODO: We need to continiously call this
    // TODO: Once we start recording the request timestamp. Only send a new request per node once
    // the time expired.
    fn replicate_log<T, I>(
        &mut self,
        share: &Share<T, I>,
        cluster: &Cluster<T, O>,
    ) -> io::Result<()>
    where
        T: Clone + Debug,
    {
        for (&peer_id, &next_index) in self.next_index.iter() {
            let request = {
                let prev_log_index = next_index - 1;
                let prev_log_term = if prev_log_index == 0 {
                    0
                } else {
                    share.log(prev_log_index).term
                };
                let entries = if share.last_log_index() >= next_index {
                    vec![share.log(next_index).clone()]
                } else {
                    Vec::new()
                };

                AppendEntriesRequest::new(
                    share.current_term,
                    share.id,
                    prev_log_index,
                    prev_log_term,
                    entries,
                    share.commit_index,
                )
            };

            let request_id = rand::random();

            self.requests.insert(
                request_id,
                (peer_id, request.prev_log_index, request.entries.len()),
            );

            send(
                cluster.channel(&peer_id),
                RaftMessage::Request(
                    request_id,
                    RaftRequest::AppendEntries(request),
                    cluster.channel(&share.id).clone(),
                ),
            )?
        }

        Ok(())
    }

    fn message<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
        request_id: &u64,
        response: &AppendEntriesResponse,
    ) -> io::Result<()>
    where
        T: Operation<I, O>,
    {
        if let Some((peer_id, prev_log_index, entries_length)) = self.requests.remove(request_id) {
            if response.success {
                // Update next index
                let next_index = self.next_index[&peer_id];
                self.next_index.insert(
                    peer_id,
                    max(next_index, prev_log_index + entries_length + 1),
                );

                // Update match index
                self.match_index
                    .insert(peer_id, self.next_index[&peer_id] - 1);
            } else {
                let new_index = self.next_index[&peer_id] - 1;
                self.next_index.insert(peer_id, new_index);
            }
        }

        // Compute the new commit index
        // TODO: Improve this to use Rust's slice
        for index in (share.commit_index + 1..=share.last_log_index()).rev() {
            if share.log(index).term == share.current_term {
                let count = self
                    .match_index
                    .values()
                    .filter(|&&peer_index| peer_index >= index)
                    .count();
                if cluster.is_majority(count + 1) {
                    share.commit_index = index;
                    break;
                }
            } else {
                break;
            }
        }

        // Apply operation to state machine
        for vector_index in share.last_applied..share.commit_index {
            let output = share.logs[vector_index]
                .operation
                .apply(&mut share.state_machine);

            share.last_applied = vector_index + 1;
            if let Some((request_id, reply)) = self.client_requests.remove(&share.last_applied) {
                // TODO: Log this request if it fails
                send(
                    &reply,
                    ClientResponse(
                        request_id,
                        AddLogResponse::new(Some(share.id), Some(output)),
                    ),
                );
            }
        }

        Ok(())
    }
}

enum Role<O> {
    Follower(Follower),
    Candidate(Candidate),
    Leader(Leader<O>),
}

impl<O> Role<O>
where
    O: Debug,
{
    fn timeout<T, I>(&mut self, share: &mut Share<T, I>, cluster: &Cluster<T, O>) -> io::Result<()>
    where
        T: Clone + Debug,
    {
        match self {
            Role::Follower(_) => self.follower_timeout(share, cluster),
            Role::Candidate(_) => self.candidate_timeout(share, cluster),
            Role::Leader(leader) => leader.replicate_log(share, cluster),
        }
    }

    fn message<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
        message: RaftMessage<T, O>,
    ) -> io::Result<()>
    where
        T: Clone + Debug + Operation<I, O>,
    {
        match self {
            Role::Follower(follower) => follower.message(share, message),
            Role::Candidate(_) => self.candidate_message(share, cluster, message),
            Role::Leader(_) => self.leader_message(share, cluster, message),
        }
    }

    fn leader_message<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
        message: RaftMessage<T, O>,
    ) -> io::Result<()>
    where
        T: Clone + Debug + Operation<I, O>,
    {
        match message {
            RaftMessage::Request(request_id, RaftRequest::Vote(request), reply) => {
                self.handle_vote_request_for_candiate_and_leader(share, request_id, request, reply)
            }
            RaftMessage::Response(_, RaftResponse::Vote(response)) => {
                if response.term > share.current_term {
                    *self = Role::Follower(Follower::new(None));
                    share.current_term = response.term;
                    share.voted_for = None;
                }

                Ok(())
            }
            RaftMessage::Request(request_id, RaftRequest::AppendEntries(request), reply) => self
                .handle_append_request_for_candiate_and_leader(
                    share, cluster, request_id, request, reply,
                ),
            RaftMessage::Response(request_id, RaftResponse::AppendEntries(response)) => {
                if response.term > share.current_term {
                    // TODO: Check the paper to make sure this is correct
                    *self = Role::Follower(Follower::new(None));
                    share.current_term = response.term;
                    share.voted_for = None;
                    Ok(())
                } else if let Role::Leader(leader) = self {
                    leader.message(share, cluster, &request_id, &response)
                } else {
                    unreachable!()
                }
            }
            RaftMessage::ClientRequest(request_id, request, reply) => {
                if let Role::Leader(leader) = self {
                    leader.client_request(share, cluster, request_id, request, reply)
                } else {
                    unreachable!()
                }
            }
        }
    }

    fn candidate_message<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
        message: RaftMessage<T, O>,
    ) -> io::Result<()>
    where
        T: Clone + Debug + Operation<I, O>,
    {
        match message {
            RaftMessage::Request(request_id, RaftRequest::Vote(request), reply) => {
                self.handle_vote_request_for_candiate_and_leader(share, request_id, request, reply)
            }
            RaftMessage::Response(request_id, RaftResponse::Vote(response)) => {
                if response.term > share.current_term {
                    *self = Role::Follower(Follower::new(None));
                    share.current_term = response.term;
                    share.voted_for = None;
                } else if response.term == share.current_term && response.vote_granted {
                    let count = if let Role::Candidate(candidate) = self {
                        candidate.update_votes(&request_id)
                    } else {
                        unreachable!()
                    };

                    if cluster.is_majority(count) {
                        *self = Role::Leader(Leader::new(share, cluster));

                        if let Role::Leader(leader) = self {
                            leader.replicate_log(share, cluster)?
                        }
                    }
                }

                Ok(())
            }
            RaftMessage::Request(request_id, RaftRequest::AppendEntries(request), reply) => self
                .handle_append_request_for_candiate_and_leader(
                    share, cluster, request_id, request, reply,
                ),
            RaftMessage::Response(request_id, RaftResponse::AppendEntries(response)) => {
                if response.term > share.current_term {
                    *self = Role::Follower(Follower::new(None));
                    self.message(
                        share,
                        cluster,
                        RaftMessage::Response(request_id, RaftResponse::AppendEntries(response)),
                    )
                } else {
                    Ok(())
                }
            }
            RaftMessage::ClientRequest(request_id, _, reply) => send(
                &reply,
                ClientResponse(request_id, AddLogResponse::new(None, None)),
            ),
        }
    }

    fn handle_append_request_for_candiate_and_leader<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
        request_id: u64,
        request: AppendEntriesRequest<T>,
        reply: mpsc::Sender<RaftMessage<T, O>>,
    ) -> io::Result<()>
    where
        T: Clone + Debug + Operation<I, O>,
    {
        if request.term >= share.current_term {
            *self = Role::Follower(Follower::new(Some(request.leader_id)));
            self.message(
                share,
                cluster,
                RaftMessage::Request(request_id, RaftRequest::AppendEntries(request), reply),
            )
        } else {
            send(
                &reply,
                RaftMessage::Response(
                    request_id,
                    RaftResponse::AppendEntries(AppendEntriesResponse::new(
                        share.current_term,
                        false,
                    )),
                ),
            )
        }
    }

    fn handle_vote_request_for_candiate_and_leader<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        request_id: u64,
        request: VoteRequest,
        reply: mpsc::Sender<RaftMessage<T, O>>,
    ) -> io::Result<()>
    where
        T: Debug,
    {
        if request.term > share.current_term {
            *self = Role::Follower(Follower::new(None));
            if let Role::Follower(follower) = self {
                follower.message(
                    share,
                    RaftMessage::Request(request_id, RaftRequest::Vote(request), reply),
                )
            } else {
                unreachable!()
            }
        } else {
            send(
                &reply,
                RaftMessage::Response(
                    request_id,
                    RaftResponse::Vote(VoteResponse::new(share.current_term, false)),
                ),
            )
        }
    }

    fn candidate_timeout<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
    ) -> io::Result<()>
    where
        T: Debug,
    {
        self.start_election(share, cluster)
    }

    fn follower_timeout<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
    ) -> io::Result<()>
    where
        T: Debug,
    {
        self.start_election(share, cluster)
    }

    fn start_election<T, I>(
        &mut self,
        share: &mut Share<T, I>,
        cluster: &Cluster<T, O>,
    ) -> io::Result<()>
    where
        T: Debug,
    {
        *self = Role::Candidate(Candidate::new(share.id));
        share.current_term += 1;
        share.voted_for = Some(share.id);

        if let Role::Candidate(candidate) = self {
            candidate.request_votes(
                share,
                cluster.peers(&share.id).into_iter(),
                cluster.channel(&share.id),
            )
        } else {
            unreachable!();
        }
    }
}

struct Share<T, I> {
    id: u8,
    current_term: u64,
    voted_for: Option<u8>,
    logs: Vec<Log<T>>,

    // Volatile
    commit_index: usize,
    last_applied: usize,
    state_machine: I,
}

impl<T, I> Share<T, I>
where
    I: Default,
{
    fn new(id: u8) -> Share<T, I> {
        Share {
            id,
            current_term: 0,
            voted_for: None,
            logs: Vec::new(),
            commit_index: 0,
            last_applied: 0,
            state_machine: I::default(),
        }
    }
}

impl<T, I> Share<T, I> {
    fn push_log(&mut self, entry: T) -> usize {
        self.logs.push(Log::new(self.current_term, entry));
        self.logs.len()
    }

    fn log(&self, index: usize) -> &Log<T> {
        &self.logs[index - 1]
    }

    fn get_log(&self, index: usize) -> Option<&Log<T>> {
        self.logs.get(index - 1)
    }

    fn last_log_index(&self) -> usize {
        self.logs.len()
    }

    fn last_log_term(&self) -> u64 {
        self.logs.last().map(|log| log.term).unwrap_or(0)
    }
}

struct Node<T, I, O> {
    role: Role<O>,
    share: Share<T, I>,
}

impl<T, I, O> Node<T, I, O>
where
    T: Clone + Debug + Operation<I, O>,
    I: Default,
    O: Debug,
{
    fn new(id: u8) -> Node<T, I, O> {
        Node {
            role: Role::Follower(Follower::new(None)),
            share: Share::new(id),
        }
    }

    fn timeout(&mut self, cluster: &Cluster<T, O>) -> io::Result<()> {
        self.role.timeout(&mut self.share, cluster)
    }

    fn message(&mut self, cluster: &Cluster<T, O>, message: RaftMessage<T, O>) -> io::Result<()> {
        self.role.message(&mut self.share, cluster, message)
    }

    fn receive_timeout(&self) -> Duration {
        // TODO: Randomize the timeout duration for followers
        // TODO: Timeout for candiate needs to be decressing
        match self.role {
            Role::Follower(_) => Duration::from_millis(160),
            Role::Candidate(_) => Duration::from_millis(160),
            Role::Leader(_) => Duration::from_millis(80),
        }
    }
}

fn send<T: Debug>(tx: &mpsc::Sender<T>, message: T) -> io::Result<()> {
    //println!("{:?} -> {:?}", thread::current().id(), message);
    tx.send(message).map_err(|msg| {
        io::Error::new(
            io::ErrorKind::Other,
            format!("Channel closed when trying to send message: {:?}", msg),
        )
    })
}

fn receive<T, I, O>(
    id: u8,
    cluster: Cluster<T, O>,
    rx: mpsc::Receiver<RaftMessage<T, O>>,
) -> io::Result<()>
where
    T: Clone + Debug + Operation<I, O>,
    I: Default,
    O: Debug,
{
    let mut state = Node::new(id);
    loop {
        match rx.recv_timeout(state.receive_timeout()) {
            Ok(message) => {
                //println!("{:?} <- {:?}", thread::current().id(), message);
                state.message(&cluster, message)?;
            }
            Err(mpsc::RecvTimeoutError::Timeout) => {
                state.timeout(&cluster)?;
            }
            Err(mpsc::RecvTimeoutError::Disconnected) => {
                return Err(io::Error::new(
                    io::ErrorKind::Other,
                    "All of the channel sender disconnected",
                ))
            }
        }
        //println!("{:?} Log {:?}", thread::current().id(), state.share.logs);
    }
}

#[derive(Clone)]
struct Cluster<T, O> {
    nodes: HashMap<u8, mpsc::Sender<RaftMessage<T, O>>>,
}

impl<T, O> Cluster<T, O> {
    fn new(
        tx1: mpsc::Sender<RaftMessage<T, O>>,
        tx2: mpsc::Sender<RaftMessage<T, O>>,
        tx3: mpsc::Sender<RaftMessage<T, O>>,
    ) -> Cluster<T, O> {
        let mut cluster = Cluster {
            nodes: HashMap::new(),
        };

        cluster.nodes.insert(1, tx1);
        cluster.nodes.insert(2, tx2);
        cluster.nodes.insert(3, tx3);

        cluster
    }

    fn peers(&self, id: &u8) -> Vec<(&u8, &mpsc::Sender<RaftMessage<T, O>>)> {
        self.nodes
            .iter()
            .filter(|&(peer_id, _)| peer_id != id)
            .collect()
    }

    fn channel(&self, id: &u8) -> &mpsc::Sender<RaftMessage<T, O>> {
        &self.nodes[id]
    }

    fn is_majority(&self, count: usize) -> bool {
        count >= self.nodes.len() / 2 + 1
    }
}

fn main() -> io::Result<()> {
    let (tx1, rx1) = mpsc::channel();
    let (tx2, rx2) = mpsc::channel();
    let (tx3, rx3) = mpsc::channel();

    let cluster = Cluster::new(tx1, tx2, tx3);
    let cluster1 = cluster.clone();
    let cluster2 = cluster.clone();
    let cluster3 = cluster.clone();

    let child1 = thread::spawn(move || receive(1, cluster1, rx1));
    let child2 = thread::spawn(move || receive(2, cluster2, rx2));
    let child3 = thread::spawn(move || receive(3, cluster3, rx3));

    let mut count = 0;
    let mut known_leader = 1;
    let (client_tx, client_rx) = mpsc::channel();
    while count < 10 {
        send(
            &cluster.nodes[&known_leader],
            RaftMessage::ClientRequest(
                rand::random(),
                AddLogRequest::new(count),
                client_tx.clone(),
            ),
        )?;

        if let Ok(ClientResponse(_, response)) = client_rx.recv() {
            if let Some(leader) = response.leader {
                if leader == known_leader {
                    println!(
                        "Got this value from the state machine: {:?}",
                        response.output
                    );
                    count += 1;
                } else {
                    known_leader = leader;
                }
            } else {
                // Cluster doesn't know the leader; wait.
                thread::sleep(Duration::from_millis(100));
            }
        } else {
            panic!("Failure waiting for the client response.");
        }
    }

    child1.join().unwrap()?;
    child2.join().unwrap()?;
    child3.join().unwrap()
}
